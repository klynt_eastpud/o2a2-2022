import random
import math
from renpy.exports import jump

FORWARD = "FORWARD"
BACKWARD = "BACKWARD"
RIGHT = "RIGHT"
LEFT = "LEFT"
DONE = "DONE"

DIRECTION_MAPPING = {
    (0, -1): FORWARD,
    (0, 1): BACKWARD,
    (1, 0): RIGHT,
    (-1, 0): LEFT,
    (0, 6): DONE
}

COORD_MAPPING = {v: k for k, v in DIRECTION_MAPPING.items()}


class Coordinate:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f"<{type(self).__name__} at ({self.x}, {self.y})>"

    def __sub__(self, other):
        return Coordinate(self.x - other.x, self.y - other.y)

    def __add__(self, other):
        return Coordinate(self.x + other.x, self.y + other.y)


class Cell(Coordinate):

    def __init__(self, x, y, dead_end=False, entrance=False, exit=False, current=False, visited=0):
        super().__init__(x, y)
        self.current = current
        self.visited = visited
        self.dead_end = dead_end
        self.entrance = entrance
        self.exit = exit
        self.proximity = None
        self.entry_point = False
        if self.entrance or self.exit:
            self.entry_point = True

        if self.dead_end:
            self.colour = "#aaf"
        elif self.exit:
            self.colour = "#ffa"
        elif self.current:
            self.colour = "#faf"
        else:
            self.colour = "#fff"


class Wall(Coordinate):
    colour = "#000"
    current = False


class Unvisited(Coordinate):
    colour = "#999"


class Maze:

    def __init__(self, width, height, dead_end_events, visited_events=None):
        self.width = width
        self.height = height
        self.dead_end_events = dead_end_events
        self.visited_events = visited_events or {}
        self.way_to_exit = None
        self.prev_visited_event = None
        self.cols = []
        self.exit = None
        self.solved = False
        self.open_directions = None

        # Denote all cells as unvisited
        for x in range(0, self.width):
            self.cols.append([Unvisited(x, y) for y in range(0, self.height)])

        self.create_maze()

    def __getitem__(self, item):
        return self.cols[item]

    @property
    def max_x(self):
        return self.width - 1

    @property
    def max_y(self):
        return self.height - 1

    @property
    def all(self):
        return [coord for row in self.rows for coord in row]

    @property
    def rows(self):
        return list(map(list, zip(*self.cols)))

    @property
    def dead_ends(self):
        return [square for square in self.all if isinstance(square, Cell) and square.dead_end]

    def update_open_directions(self):
        adjacent_cells = self.get_adjacent_cells(self.current, include_edges=not self.current.entrance)
        directions = set()
        for cell in adjacent_cells:
            coord_diff = cell - self.current
            open_dir = DIRECTION_MAPPING[(coord_diff.x, coord_diff.y)]
            # attempt to fix a key error (0,6) that occasionally happens when reaching the exit
            if open_dir == DONE:
                self.solved = True
            directions.add(open_dir)
        self.open_directions = directions

    def update_current(self, new):
        self.current.current = False
        self.current = new
        new.current = True

    def move_player(self, direction):
        coord_diff = COORD_MAPPING[direction]
        target_coords = [self.current.x + coord_diff[0], self.current.y + coord_diff[1]]
        target = self.cols[target_coords[0]][target_coords[1]]
        if target.exit:
            self.solved = True
        if isinstance(target, Cell):
            self.update_current(target)
            self.update_open_directions()

    def move(self, direction):
        # keep going until we hit a junction or a wall
        # have to move to the first square before we can loop
        self.move_player(direction)

        if direction in {FORWARD, BACKWARD}:
            # check if we can keep going
            while (
                len(self.open_directions) <= 2 and
                direction in self.open_directions and
                (RIGHT not in self.open_directions or LEFT not in self.open_directions)
            ):
                self.move_player(direction)

        elif direction in {LEFT, RIGHT}:
            while (
                len(self.open_directions) <= 2 and
                direction in self.open_directions and
                (FORWARD not in self.open_directions or BACKWARD not in self.open_directions)
            ):
                self.move_player(direction)

        if self.current.dead_end and self.dead_end_events:
            if not self.current.visited:
                jump_label = self.dead_end_events[0]
                self.dead_end_events.remove(jump_label)
                self.current.visited += 1
                jump(jump_label)

        elif self.current.visited > 1:
            jump_label = self.visited_events.get(self.current.visited)
            self.current.visited += 1
            # don't play the same event twice in a row
            if jump_label and self.prev_visited_event != jump_label:
                self.prev_visited_event = jump_label
                jump(jump_label)

        else:
            self.current.visited += 1

    def opposite_squares(self, square):
        squares = []
        adjacent_cells = self.get_adjacent_cells(square)

        if adjacent_cells:
            for c in adjacent_cells:
                cell = self.cols[c.x][c.y]
                opposite_forward = square + (cell - square)
                opposite_back = square - (cell - square)

                squares.append(self.cols[opposite_forward.x][opposite_forward.y])
                squares.append(self.cols[opposite_back.x][opposite_back.y])

        return squares

    def is_edge(self, coord):
        return not (coord.x > 0 and coord.x < self.max_x and coord.y > 0 and coord.y < self.max_y)

    def add_to_maze(self, obj, **kwargs):
        self.cols[obj.x][obj.y] = obj
        return obj

    def get_adjacent_coords(self, x, y, include_edges=False):
        all = [
            [x-1, y],  # left
            [x+1, y],  # right
            [x, y-1],  # top
            [x, y+1],  # bottom
        ]
        if include_edges:
            return all
        return [
            [x, y] for x, y in all
            if x > 0 and x < self.max_x and
            y > 0 and y < self.max_y
        ]

    def get_adjacent_squares(self, coord, **kwargs):
        coords = self.get_adjacent_coords(coord.x, coord.y, **kwargs)
        return [
            self.cols[x][y] for x, y in coords
        ]

    def get_adjacent_cells(self, coord, **kwargs):
        return [
            c for c in self.get_adjacent_squares(coord, **kwargs) if isinstance(c, Cell)
        ]

    def create_maze(self):
        # Randomize starting point and set it as a cell
        starting_height = random.randint(1, self.max_y - 1)
        starting_width = random.randint(1, self.max_x - 1)

        # Mark it as a cell and add surrounding walls to the list
        self.add_to_maze(Cell(starting_width, starting_height))
        self.walls = set()

        for x, y in self.get_adjacent_coords(starting_width, starting_height):
            self.walls.add(Wall(x, y))
            self.add_to_maze(Wall(x, y))

        while self.walls:
            # Pick a random wall
            rand_wall = random.choice(list(self.walls))

            adjacent_squares = self.get_adjacent_squares(rand_wall)

            # find the square on the opposite side to the connecting cell
            adjacent_cells = self.get_adjacent_cells(rand_wall)

            if len(adjacent_cells) == 1:
                for o_square in self.opposite_squares(rand_wall):

                    if isinstance(o_square, Unvisited):
                        if not self.is_edge(o_square):
                            new_wall = Wall(o_square.x, o_square.y)
                            self.add_to_maze(new_wall)
                            self.walls.add(new_wall)
                        self.add_to_maze(Cell(rand_wall.x, rand_wall.y))

                        for a_square in adjacent_squares:
                            if isinstance(a_square, Unvisited):
                                new_wall = Wall(a_square.x, a_square.y)
                                self.add_to_maze(new_wall)
                                self.walls.add(new_wall)

            self.walls.remove(rand_wall)

        # Mark the remaining unvisited cells as walls
        for coord in self.all:
            if isinstance(coord, Unvisited):
                self.add_to_maze(Wall(coord.x, coord.y))

        # Set entrance and exit
        # Exit is at the top edge
        rand_square = random.choice([s for s in self.rows[1] if isinstance(s, Cell)])
        self.exit = self.add_to_maze(Cell(rand_square.x, 0, exit=True))

        # Entrance is at the bottom edge
        rand_square = random.choice([s for s in self.rows[-2] if isinstance(s, Cell)])
        self.current = self.add_to_maze(Cell(rand_square.x, self.max_y, entrance=True, current=True, visited=1))
        self.update_open_directions()

        # Mark the dead ends
        for square in self.all:
            if isinstance(square, Cell):
                diff_x = square.x - self.exit.x
                diff_y = square.y - self.exit.y
                amount = math.sqrt(math.pow(diff_x, 2) + math.pow(diff_y, 2))
                max_alpha = 0.5
                add = 0.1
                prox = add + (max_alpha - ((amount / self.height) * max_alpha))
                square.proximity = (prox * (prox * 0.1)) * 10

                if self.exit.x > square.x:
                    self.way_to_exit = RIGHT
                elif self.exit.x < square.x:
                    self.way_to_exit = LEFT

                if not square.entry_point:
                    s_cells = self.get_adjacent_cells(square, include_edges=True)
                    if not any([s.entry_point for s in s_cells]) and len(s_cells) == 1:
                        if not square.entry_point:
                            square.dead_end = True
