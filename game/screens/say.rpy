
## Say screen ##################################################################
##
## The say screen is used to display dialogue to the player. It takes two
## parameters, who and what, which are the name of the speaking character and
## the text to be displayed, respectively. (The who parameter can be None if no
## name is given.)
##
## This screen must create a text displayable with id "what", as Ren'Py uses
## this to manage text display. It can also create displayables with id "who"
## and id "window" to apply style properties.
##
## https://www.renpy.org/doc/html/screen_special.html#say

define box_border = 6
define box_offset = (-box_border/2, -box_border/2)
define saybox_height = 200
define saybox_width = 70


screen say(who, what):
    style_prefix "say"

    use quick_menu

    frame:
        style "dialogue_box"
        offset box_offset
        ysize dialogue_box_height + box_border
        xsize window_width + box_border
        background "border_image"

    window:
        id "dialogue_box"
        style "dialogue_box"
        if alt_saybox:
            background Solid("#000")
        else:
            background Solid("#fff")

        if who is not None:

            frame:
                style "namebox"
                offset box_offset
                xsize saybox_height + box_border
                ysize saybox_width + box_border

                background "border_image"

            window:
                id "namebox"
                style "namebox"

                if alt_saybox:
                    background Solid("#000")
                else:
                    background Solid("#fff")

                if alt_saybox:
                    text who id "who" color "#fff"
                else:
                    text who id "who" color "#000"

        if alt_saybox:
            text what id "what" color "#fff"
        else:
            text what id "what" color "#000"

    ## If there's a side image, display it above the text. Do not display on the
    ## phone variant - there's no room.
    if not renpy.variant("small"):
        add SideImage() xalign 0.0 yalign 1.0


## Make the namebox available for styling through the Character object.
init python:
    config.character_id_prefixes.append('namebox')

style dialogue_box is default
style say_label is default
style say_dialogue is default
style say_thought is say_dialogue

style namebox is default
style namebox_shadow is default
style namebox_label is say_label

define window_width = 1200

define dialogue_box_height = 300

style dialogue_box:
    xpos int((screen_width - window_width) / 2)
    ypos screen_height - dialogue_box_height - 50
    ysize dialogue_box_height
    xsize window_width


style namebox:
    xpos -50
    ypos -30

    ## The horizontal alignment of the character's name. This can be 0.0 for left-
    ## aligned, 0.5 for centered, and 1.0 for right-aligned.
    xsize saybox_height
    ysize saybox_width

    padding (10, 10)

style say_label:
    ## The position, width, and alignment of the label giving the name of the
    ## speaking character.
    xpos 10
    xanchor 0
    yanchor 0.5
    xsize None
    xalign 0.0
    yalign 0.5

define xborder = 50
define yborder = 30

style say_dialogue:
    xoffset xborder
    yoffset yborder
    ypos 50

    ## The maximum width of dialogue text, in pixels.
    xsize window_width - 100

    ## The horizontal alignment of the dialogue text. This can be 0.0 for left-
    ## aligned, 0.5 for centered, and 1.0 for right-aligned.
    xalign 0.0

    adjust_spacing False
