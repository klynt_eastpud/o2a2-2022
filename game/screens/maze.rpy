
screen maze():

    zorder 200

    $ squaresize = 50

    if "FORWARD" in maze.open_directions:
        add "particles_top":
            alpha maze.current.proximity
    if "RIGHT" in maze.open_directions:
        add "particles_right":
            alpha maze.current.proximity
    if "LEFT" in maze.open_directions:
        add "particles_left":
            alpha maze.current.proximity

    frame:
        style "help_box"
        offset box_offset
        ysize help_box_height + box_border
        xsize help_box_width + box_border
        background "border_image"

    window:
        style "help_box"
        background Solid("#000")
        text "Help Karel navigate through the automaton's mind and find Josef!" style "help_text"

    if config.debug:

        window:
            style_prefix "quick"
            xpos screen_width - 400
            ypos screen_height - 400
            xoffset -20
            yoffset -20

            textbutton "Skip" action Jump("after_maze")

        vbox:
            spacing 0
            xalign 1.0
            yalign 1.0
            xoffset -20
            yoffset -20

            for row in maze.rows:
                hbox:
                    spacing 0

                    for square in row:
                        frame:
                            ysize squaresize
                            xsize squaresize
                            background square.colour

                            if square.current:
                                text "[square.visited]" color "#000"

style help_text is default
style help_box is default

define help_box_width = 400
define help_box_height = 200

style help_box:
    xpos 100
    ypos screen_height - help_box_height - 100
    ysize help_box_height
    xsize help_box_width


style help_text:
    xoffset xborder
    yoffset yborder
    size 25

    ## The maximum width of dialogue text, in pixels.
    xsize help_box_width - 100

    ## The horizontal alignment of the dialogue text. This can be 0.0 for left-
    ## aligned, 0.5 for centered, and 1.0 for right-aligned.
    xalign 0.0
