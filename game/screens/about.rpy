
## About screen ################################################################
##
## This screen gives credit and copyright information about the game and Ren'Py.
##
## There's nothing special about this screen, and hence it also serves as an
## example of how to make a custom screen.

screen about():

    tag menu

    ## This use statement includes the game_menu screen inside this one. The
    ## vbox child is then included inside the viewport inside the game_menu
    ## screen.
    use game_menu(_("About")):

        style_prefix "about"
        window:
            ypadding 100
            xpadding 0

            vbox:
                spacing 60

                vbox:
                    label "[config.name!t]"
                    text _("Version [config.version!t]"):
                        yoffset 10

                ## gui.about is usually set in options.rpy.

                vbox:
                    label "Credits"
                    text "[gui.about!t]\n":
                        yoffset 10
                    text _("Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]")
                
                vbox:
                    label "Warning!"
                    text "[gui.warnings!t]":
                        yoffset 10


style about_label is gui_label
style about_label_text is gui_label_text
style about_text is gui_text

style about_label_text:
    size gui.label_text_size

style about_text:
    size 22
