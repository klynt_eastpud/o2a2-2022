
## Main Menu screen ############################################################
##
## Used to display the main menu when Ren'Py starts.
##
## https://www.renpy.org/doc/html/screen_special.html#main-menu

image ctc_title:
    "ctc"
    zoom 2.2
    yoffset 15
    xoffset -10

screen main_menu():

    ## This ensures that any other menu screen is replaced.
    tag menu

    # add gui.main_menu_background

    ## This empty frame darkens the main menu.
    # frame:
    #     style "main_menu_frame"

    ## The use statement includes another screen inside this one. The actual
    ## contents of the main menu are in the navigation screen.
    use navigation

    if gui.show_name:

        window:
            style "main_menu_window"

            text "[gui.title_log!t]\n":
                color "#444"
                size 20

            text "❯ [config.name!t] {image=ctc_title}":
                style "main_menu_title"


style main_menu_window is window
style main_menu_text is gui_text
style main_menu_title is main_menu_text
style main_menu_version is main_menu_text

style main_menu_window:
    xalign 0.0
    padding (50, 100)
    yalign 1.0
    spacing 5

style main_menu_text:
    ## The position of the main menu text.
    xalign 0.0
    yalign 1.0

style main_menu_title:
    ## The size of the game's title.
    size 75

# style main_menu_version:
#     properties gui.text_properties("version")
