
## Quick Menu screen ###########################################################
##
## The quick menu is displayed in-game to provide easy access to the out-of-game
## menus.

define qmenu_height = 300
define qmenu_width = 200

define lower = 0.4
define upper = 0.6

image border_image:
    contains:
        "black"
        alpha upper
        linear 0.5 alpha lower
        linear 0.5 alpha upper
        repeat
    contains:
        "white"
        alpha lower
        linear 0.5 alpha upper
        linear 0.5 alpha lower
        repeat

screen quick_menu():

    ## Ensure this appears on top of other screens.
    zorder 100


    frame:
        style "qmenu_box"
        offset box_offset
        ysize qmenu_height + box_border
        xsize qmenu_width + box_border

        background "border_image"

    window:
        style "qmenu_box"

        if alt_saybox:
            background Solid("#000")
        else:
            background Solid("#fff")

        vbox:
            style_prefix "quick"
            spacing 0

            # textbutton _("Back") action Rollback()
            textbutton _("History") action ShowMenu('history')
            textbutton _("Skip") action Skip() alternate Skip(fast=True, confirm=True)
            textbutton _("Auto") action Preference("auto-forward", "toggle")
            textbutton _("Save") action ShowMenu('save')
            textbutton _("Load") action ShowMenu('load')
            # textbutton _("Q.Save") action QuickSave()
            # textbutton _("Q.Load") action QuickLoad()
            textbutton _("Prefs") action ShowMenu('preferences')


## This code ensures that the quick_menu screen is displayed in-game, whenever
## the player has not explicitly hidden the interface.
# init python:
#     config.overlay_screens.append("quick_menu")

default quick_menu = True

style qmenu_box is default
style quick_button is default
style quick_button_text is button_text

style qmenu_box:
    # properties gui.button_properties("quick_button")
    xpos int((screen_width - window_width)/2) + window_width + 50
    ypos screen_height - qmenu_height - 50
    xsize qmenu_width
    ysize qmenu_height

    padding (30, 30)

style quick_button:
    # properties gui.button_properties("quick_button")
    xpos 0

style quick_button_text:
    # properties gui.button_text_properties("quick_button")
    size 30
    idle_color gui.idle_small_color
    selected_color gui.accent_color
