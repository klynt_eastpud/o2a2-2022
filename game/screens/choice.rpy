
## Choice screen ###############################################################
##
## This screen is used to display the in-game choices presented by the menu
## statement. The one parameter, items, is a list of objects, each with caption
## and action fields.
##
## https://www.renpy.org/doc/html/screen_special.html#choice

define edge_offset = 100

screen choice(items, maze_choice=False):
    style_prefix "choice"

    if maze_choice:
        use maze
        # arrange the choices according to direction
        vbox:
            xalign 0.5
            xanchor 0.5
            ypos screen_height - 300
            yanchor 0
            spacing -50

            for i in items:
                if i.args[0] == "FORWARD":
                    textbutton "▲" action i.action style "direction_button"

            hbox:
                spacing 100
                for i in items:
                    if i.args[0] == "RIGHT":
                        textbutton "▶︎" action i.action style "direction_button"

                    elif i.args[0] == "LEFT":
                        textbutton "◀︎" action i.action style "direction_button"

            for i in items:
                if i.args[0] == "BACKWARD":
                    textbutton "▼" action i.action style "direction_button"


    else:
        vbox:
            yalign 0.5
            for i in items:
                textbutton i.caption action i.action


style choice_vbox is vbox
style choice_button is button
style choice_button_text is button_text

style choice_vbox:
    xalign 0.5
    yanchor 0.5

    spacing gui.choice_spacing

define button_size = 200

style direction_button is default:
    xalign 0.5
    xanchor 0.5
    yanchor 0.5
    background None

style direction_button_text is default:
    font gui.text_font
    size 100
    xalign 0.5
    idle_color "#cccccc"
    hover_color "#ffffff"
    insensitive_color "#444444"
    outlines [ (5, "#fff", 0, 0) ]
    insensitive_outlines [ (5, "#000", 0, 0) ]


style choice_button is default:
    properties gui.button_properties("choice_button")

style choice_button_text is default:
    properties gui.button_text_properties("choice_button")
