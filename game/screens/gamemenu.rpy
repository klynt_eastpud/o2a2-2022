
## Game Menu screen ############################################################
##
## This lays out the basic common structure of a game menu screen. It's called
## with the screen title, and displays the background, title, and navigation.
##
## The scroll parameter can be None, or one of "viewport" or "vpgrid". When
## this screen is intended to be used with one or more children, which are
## transcluded (placed) inside it.

screen game_menu(title):

    style_prefix "game_menu"

    frame:
        style "game_menu_outer_frame"

        if main_menu:
            background "#000"
        else:
            background None
            add "black":
                alpha 0.9

        frame:
            style "game_menu_content_frame"

            transclude

    use navigation

    # label title

    if main_menu:
        key "game_menu" action ShowMenu("main_menu")


style game_menu_outer_frame is empty
style game_menu_content_frame is empty
style game_menu_viewport is gui_viewport
style game_menu_side is gui_side
style game_menu_scrollbar is gui_vscrollbar

style game_menu_label is gui_label
style game_menu_label_text is gui_label_text

style game_menu_outer_frame:
    top_padding 60
    xsize screen_width
    ysize screen_height

style game_menu_content_frame:
    xalign 0.5
    xsize int(screen_width / 3) * 2
    # ypadding 100
    # background "#555"

style game_menu_viewport:
    xsize 1380

style game_menu_vscrollbar:
    unscrollable gui.unscrollable

style game_menu_side:
    spacing 15

style game_menu_label:
    # xpos 75
    ysize 180

style game_menu_label_text:
    size 75
    color gui.accent_color
    yalign 1.0
