
################################################################################
## Main and Game Menu Screens
################################################################################

## Navigation screen ###########################################################
##
## This screen is included in the main and game menus, and provides navigation
## to other menus, and to start the game.

screen navigation():

    window:
        background "#fff"
        xsize screen_width

        hbox:
            style_prefix "navigation"

            if main_menu:

                frame:
                    xsize 200
                    ysize 60
                    xpadding 25
                    background "#ccc"

                    text "V [config.version]":
                        yalign 0.5
                        xalign 0.5
                        color "#444"

                button action Start():
                    text _("Start")

            else:

                button action Return():
                    text _("Return"):
                        yalign 0.5
                        xalign 0.5

                button action MainMenu():
                    text _("Title")

                button action ShowMenu("history"):
                    text _("History")

                button action ShowMenu("save"):
                    text _("Save")

            button action ShowMenu("load"):
                text _("Load")

            button action ShowMenu("preferences"):
                text _("Config")

            if _in_replay:

                button action EndReplay(confirm=True):
                    text _("End Replay")

            button action ShowMenu("about"):
                text _("About")

            if renpy.variant("pc") or (renpy.variant("web") and not renpy.variant("mobile")):

                ## Help isn't necessary or relevant to mobile devices.
                button action ShowMenu("help"):
                    text _("Help")

            if renpy.variant("pc"):

                ## The quit button is banned on iOS and unnecessary on Android and
                ## Web.
                button action Quit(confirm=not main_menu):
                    text _("Quit")


style navigation_button is gui_button
style navigation_button_text is gui_button_text

## The position of the left side of the navigation buttons, relative to the left
## side of the screen.
style navigation:
    xpos 0

style navigation_button:
    background None
    idle_background None
    hover_background "#aaa"
    selected_background "#444"
    insensitive_background "#ccc"
    xsize None
    ysize 60
    xpadding 25

## Buttons in the navigation section of the main and game menus.
define gui.navigation_spacing = 10

style navigation_text:
    # font gui.text_font
    # size gui.text_size
    # xalign 0.5
    color "#444"
    hover_color "#444"
    insensitive_color "#ccc"
    selected_color "#fff"
