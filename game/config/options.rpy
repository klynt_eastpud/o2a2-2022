﻿## This file contains options that can be changed to customize your game.
##
## Lines beginning with two '#' marks are comments, and you shouldn't uncomment
## them. Lines beginning with a single '#' mark are commented-out code, and you
## may want to uncomment them when appropriate.


## Basics ######################################################################

## A human-readable name of the game. This is used to set the default window
## title, and shows up in the interface and error reports.
##
## The _() surrounding the string marks it as eligible for translation.

define config.name = _("Your Ghost in the Machine")
define config.menu_include_disabled = True
define config.gl2 = True

## Determines if the title given above is shown on the main menu screen. Set
## this to False to hide the title.

define gui.show_name = True

define config.debug = False


## The version of the game.

define config.version = "1.0"


## Text that is placed on the game's about screen. Place the text between the
## triple-quotes, and leave a blank line between paragraphs.

define gui.about = _p("""
Made by {a=https://supercircuit.itch.io/}supercircuit{/a} for O2A2 Jam 2022

Music: {a=https://www.chosic.com/download-audio/24730/}Marsel Minga{/a}

Sound: {a=https://freesound.org/}Freesound.org{/a}
""")

define gui.warnings = _p("""
Sound effect may be harsh on your ears - adjust your game volume through the config menu appropriately.

This game contains flickering/flashing images.
""")

define gui.title_log = _p("""
==> Downloading https://alkg.io/v2/8gd7222hj/automata-28789/blobs/sha256:1f50bf80583bd436c9542d4fa5ad47df0ef0f0bea22ae710c4f04c42d7560bca

################################################################################################ 100.0%

==> Installing automata-28789-2.6.8_1.309915985.tar.gz

==> Downloading https://alkg.io/v2/24jkp52/automata-core-3498/blobs/sha256:2ae710c4f04c42d7560bca1f50bf80583bd436c9542d4fa5ad47df0ef0f0bea2

################################################################################################ 100.0%

==> Installing automata-core-3498-1.2.13498743.tar.gz

==> Downloading https://alkg.io/v2/38fh9dn72/automata-security-2881/blobs/sha256:2d7560bca1f50bf80583bd436c92ae710c4f04c4542d4fa5ad47df0ef0f0bea2

################################################################################################ 100.0%

==> Installing automata-security-2881-5.0.1983728.tar.gz

==> Downloading https://alkg.io/v2/29bf897d/automata-security-39187/blobs/sha256:60bca1f50bf80583bd4362d75c92ae710c4f04c4542d4fa5ad47df0ef0f0bea2

################################################################################################ 100.0%

==> Installing automata-cognitive-9818-2.26.1_4.2245871.tar.gz

Updated 6 packages (automata/core, automata/security, automata/motor, automata/cognitive and automata/services).

==> New Packages

Last login: Sun May  1 23:41:05 on fcce78ac678e

/usr/local/bin/automata4.9: Error while finding module specification for 'automata.hook_loader' (ModuleNotFoundError: No module named 'automata')

automata.sh: There was a problem running the initialization hooks.
""")


## A short name for the game used for executables and directories in the built
## distribution. This must be ASCII-only, and must not contain spaces, colons,
## or semicolons.

define build.name = "your_ghost_in_the_machine"


## Sounds and music ############################################################

## These three variables control, among other things, which mixers are shown
## to the player by default. Setting one of these to False will hide the
## appropriate mixer.

define config.has_sound = True
define config.has_music = True
define config.has_voice = False


## To allow the user to play a test sound on the sound or voice channel,
## uncomment a line below and use it to set a sample sound to play.

# define config.sample_sound = "sample-sound.ogg"
# define config.sample_voice = "sample-voice.ogg"


## Uncomment the following line to set an audio file that will be played while
## the player is at the main menu. This file will continue playing into the
## game, until it is stopped or another file is played.

# define config.main_menu_music = "main-menu-theme.ogg"


## Transitions #################################################################
##
## These variables set transitions that are used when certain events occur.
## Each variable should be set to a transition, or None to indicate that no
## transition should be used.

## Entering or exiting the game menu.

define config.enter_transition = Dissolve(0.5)
define config.exit_transition = Dissolve(0.5)


## Between screens of the game menu.

define config.intra_transition = Dissolve(.25)


## A transition that is used after a game has been loaded.

define config.after_load_transition = Dissolve(.25)


## Used when entering the main menu after the game has ended.

define config.end_game_transition = Dissolve(1.0)


## A variable to set the transition used when the game starts does not exist.
## Instead, use a with statement after showing the initial scene.


## Window management ###########################################################
##
## This controls when the dialogue window is displayed. If "show", it is always
## displayed. If "hide", it is only displayed when dialogue is present. If
## "auto", the window is hidden before scene statements and shown again once
## dialogue is displayed.
##
## After the game has started, this can be changed with the "window show",
## "window hide", and "window auto" statements.

define config.window = "auto"


## Transitions used to show and hide the dialogue window

define config.window_show_transition = Dissolve(.2)
define config.window_hide_transition = Dissolve(.2)


## Preference defaults #########################################################

## Controls the default text speed. The default, 0, is infinite, while any other
## number is the number of characters per second to type out.

default preferences.text_cps = 100


## The default auto-forward delay. Larger numbers lead to longer waits, with 0
## to 30 being the valid range.

default preferences.afm_time = 15


## Save directory ##############################################################
##
## Controls the platform-specific place Ren'Py will place the save files for
## this game. The save files will be placed in:
##
## Windows: %APPDATA\RenPy\<config.save_directory>
##
## Macintosh: $HOME/Library/RenPy/<config.save_directory>
##
## Linux: $HOME/.renpy/<config.save_directory>
##
## This generally should not be changed, and if it is, should always be a
## literal string, not an expression.

define config.save_directory = "your_ghost_in_the_machine-1658496721"


## Icon ########################################################################
##
## The icon displayed on the taskbar or dock.

# define config.window_icon = "icon.png"


## Build configuration #########################################################
##
## This section controls how Ren'Py turns your project into distribution files.

init python:

    ## The following functions take file patterns. File patterns are case-
    ## insensitive, and matched against the path relative to the base directory,
    ## with and without a leading /. If multiple patterns match, the first is
    ## used.
    ##
    ## In a pattern:
    ##
    ## / is the directory separator.
    ##
    ## * matches all characters, except the directory separator.
    ##
    ## ** matches all characters, including the directory separator.
    ##
    ## For example, "*.txt" matches txt files in the base directory, "game/
    ## **.ogg" matches ogg files in the game directory or any of its
    ## subdirectories, and "**.psd" matches psd files anywhere in the project.

    ## Classify files as None to exclude them from the built distributions.

    build.classify('**~', None)
    build.classify('**.bak', None)
    build.classify('**/.**', None)
    build.classify('**/#**', None)
    build.classify('**/thumbs.db', None)

    ## To archive files, classify them as 'archive'.

    build.classify('game/**.png', 'archive')
    build.classify('game/**.mp3', 'archive')
    build.classify('game/**.py', 'archive')
    build.classify('game/**.rpy', 'archive')
    build.classify('game/**.ttf', 'archive')
    # build.classify('game/**.jpg', 'archive')

    ## Files matching documentation patterns are duplicated in a mac app build,
    ## so they appear in both the app and the zip file.

    build.documentation('*.html')
    build.documentation('*.txt')


## A Google Play license key is required to download expansion files and perform
## in-app purchases. It can be found on the "Services & APIs" page of the Google
## Play developer console.

# define build.google_play_key = "..."


## The username and project name associated with an itch.io project, separated
## by a slash.

# define build.itch_project = "renpytom/test-project"
