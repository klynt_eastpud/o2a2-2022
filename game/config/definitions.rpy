init -1:

    image ctc_black = Solid("#000", xsize=20, ysize=30)
    image ctc_white = Solid("#fff", xsize=20, ysize=30)
    image ctc_line_black = Solid("#000", xsize=20, ysize=5)
    image ctc_line_white = Solid("#fff", xsize=20, ysize=5)

    define ctc_line_offset = 20
    define ctc_offset = 5

    if renpy.macintosh:
        $ ctc_line_offset = 50
        $ ctc_offset = 10

    image ctc_line:
        "ctc_line_black"
        yoffset ctc_line_offset
        0.5
        "ctc_line_white"
        0.5
        repeat

    image ctc:
        "ctc_black"
        yoffset ctc_offset
        xoffset 10
        0.5
        "ctc_white"
        0.5
        repeat

    # CHARACTERS
    $ default = Character(None,
        what_prefix=u"\u201C",
        what_suffix=u"\u201D",
        ctc="ctc",
        ctc_pause="ctc_line",
        )

    $ Karel = Character("Karel", kind=default)
    $ Josef = Character("Josef", kind=default)
    $ JosefNVL = Character(None, kind=nvl)
    $ Someone = Character("???", kind=default)
    $ N = Character(None, ctc="ctc", ctc_pause="ctc_line")
    $ NVL = Character(None, kind=nvl)

    ## EFFECTS
    $ flashwhite = Fade(0.1, 0.0, 0.4, color='#fff')
    $ flashred = Fade(0.1, 0.0, 0.4, color='#ff0000')
    $ flashblack = Fade(0.1, 0.0, 0.4, color='#000')

    # COLOURS
    image white = Solid("#fff")
    image black = Solid("#000")
    image grey = Solid("#ccc")
    image lightgrey = Solid("#eee")
    image blue = Solid("#013")
    image red = Solid("#f00")

    # TRANSITIONS
    define dissolve = Dissolve(0.25)
    define wiperight = CropMove(0.25, "wiperight")
    define wipeup = CropMove(0.25, "wipeup")
    define slowwipeup = CropMove(1.0, "wipeup")
    define long_dissolve = Dissolve(0.5)
    define fade = Fade(0.5, 0.25, 0.5)
    define long_fade = Fade(1.0, 0.5, 1.0)
    define short_pixellate = Pixellate(0.25, 5)
    define pixellate = Pixellate(1.0, 7)
    define long_pixellate = Pixellate(2.0, 10)
    define zoom_in_fast = ImageDissolve("zoom.png", 0.5, ramplen=256)
    define zoom_in = ImageDissolve("zoom.png", 1.0, ramplen=256)
    define zoom_in_slow = ImageDissolve("zoom.png", 3.0, ramplen=256)

    # BACKGROUND

    image lab_light:
        "lab"
        matrixcolor BrightnessMatrix(0.2)

    image lab_blur_light:
        "lab"
        zoom 1.1
        align (0.5, 0.5)
        matrixcolor BrightnessMatrix(0.3)
        blur 200

    image lab_blur_dark:
        "lab"
        zoom 1.1
        align (0.5, 0.5)
        xzoom -1.0
        matrixcolor BrightnessMatrix(-0.7)
        blur 200
