
# TRANSFORMS

define middle_align = (0.0, 0.05)
define middle_zoom = 0.45

define middle_far_align = (0.5, 0.2)
define middle_far_zoom = 0.17

transform middle_close:
    yoffset 0
    align (0.615, 0.09)
    zoom 1.6

transform middle_very_close:
    yoffset 0
    align (0.6, 0.09)
    zoom 2.25

define shake_distance = 10
transform middle_very_close_shake:
    yoffset 0
    align (0.6, 0.1)
    zoom 2.25
    xoffset 0
    1.0
    xoffset shake_distance
    0.1
    xoffset 0
    2.0
    xoffset shake_distance
    0.1
    xoffset 0
    0.1
    xoffset shake_distance
    0.1
    xoffset 0
    0.1
    xoffset shake_distance
    0.1
    xoffset 0
    2.0
    repeat

define shake_distance2 = 2

transform middle_shake:
    offset (0, 0)
    align middle_align
    zoom middle_zoom
    1.0
    offset (shake_distance2, -shake_distance2)
    0.1
    offset (0, 0)
    2.0
    offset (shake_distance2, -shake_distance2)
    0.1
    offset (0, 0)
    0.1
    offset (shake_distance2, -shake_distance2)
    0.1
    offset (0, 0)
    0.1
    offset (shake_distance2, -shake_distance2)
    0.1
    offset (0, 0)
    2.0
    repeat

transform middle:
    yoffset 0
    align middle_align
    zoom middle_zoom

transform middle_far:
    yoffset 0
    align middle_far_align
    zoom middle_far_zoom

transform middle_very_far:
    yoffset 0
    xalign 0.5
    yalign 0.4
    zoom 0.07

transform middle_pan_up:
    xalign 0.75 zoom 0.2 yalign 1.5
    ease 5.0 yalign 0.08

transform blurry:
    blur 50.0

transform very_blurry:
    blur 100.0

define laugh_distance = -5

transform middle_laughing:
    align middle_align
    zoom middle_zoom
    yoffset 0
    linear 0.1 yoffset laugh_distance
    linear 0.1 yoffset 0
    pause 1.5
    linear 0.1 yoffset laugh_distance
    linear 0.1 yoffset 0
    linear 0.1 yoffset laugh_distance
    linear 0.1 yoffset 0
    pause 1.0
    repeat

transform middle_bounce:
    align middle_align
    zoom middle_zoom
    yoffset 0
    linear 0.05 yoffset -5
    linear 0.05 yoffset 0

transform middle_jump:
    align middle_align
    zoom middle_zoom
    yoffset 0
    linear 0.05 yoffset -20
    linear 0.05 yoffset 0

transform zoom_face_laughing:
    xalign 0.6
    yalign 0.08
    yoffset 0
    linear 0.1 yoffset laugh_distance
    linear 0.1 yoffset 0
    pause 1.5
    linear 0.1 yoffset laugh_distance
    linear 0.1 yoffset 0
    linear 0.1 yoffset laugh_distance
    linear 0.1 yoffset 0
    pause 1.0
    repeat

define breath_distance = -10

transform zoom_face_breathing:
    xalign 0.6
    yalign 0.08
    yoffset 0
    ease 1.0 yoffset breath_distance
    pause 1.0
    ease 0.5 yoffset 0
    pause 2.0
    repeat

transform middle_breathing:
    align middle_align
    zoom middle_zoom
    ease 1.0 yoffset breath_distance
    pause 1.0
    ease 0.5 yoffset 0
    pause 2.0
    repeat
