
image eyes_shake:
    "eyes_wide2_irises"
    xoffset -1
    0.05
    xoffset 1
    0.05
    repeat

define m_alpha = 0.5

image mouth_shake:
    "mouth_scream"
    pos (20, 10)
    alpha m_alpha
    0.05
    alpha 0
    1.0
    alpha m_alpha
    0.05
    alpha 0.0
    0.05
    alpha m_alpha
    0.05
    alpha 0
    2.0
    repeat

layeredimage josef:

    group body:
        attribute body_normal default:
            "body"
        attribute glitched:
            "glitch1"

    group eyes:
        attribute e_normal default:
            "eyes_normal"
        attribute e_closed_smile:
            "eyes_closed_smile"
        attribute e_closed:
            "eyes_closed"
        attribute e_smile:
            "eyes_smile"
        attribute e_cry:
            "eyes_cry"
        attribute e_side:
            "eyes_side"
        attribute e_wide:
            "eyes_wide"
        attribute e_wide2:
            "eyes_wide2"

    group eyes2:
        attribute e_wide2:
            "eyes_shake"

    group mouth:
        attribute m_normal default:
            "mouth_normal"
        attribute m_open:
            "mouth_open"
        attribute m_smile:
            "mouth_smile"
        attribute m_scream:
            "mouth_scream"

    group mouth2:
        attribute m_scream:
            "mouth_shake"

    if glasses:
        "glasses"

    group eyebrows_colour:
        attribute eb_normal default:
            "eyebrows_normal"
        attribute eb_frown:
            "eyebrows_frown2"
        # attribute eb_frown2:
        #     "eyebrows_frown2"
        attribute eb_raised:
            "eyebrows_raised"
        attribute eb_sad:
            "eyebrows_sad"

    group hair:
        attribute hair default:
            "hair"

    group eyebrows_lines:
        attribute eb_normal default:
            "eyebrows_normal_lines"
        attribute eb_frown:
            "eyebrows_frown2_lines"
        # attribute eb_frown2:
        #     "eyebrows_frown2_lines"
        attribute eb_raised:
            "eyebrows_raised_lines"
        attribute eb_sad:
            "eyebrows_sad_lines"

    group effects:
        attribute static:
            "static"
            additive True

    group effects3:
        attribute pixels1:
            "mosaic1"
            alpha 0.2
            additive True
        attribute pixels2:
            "mosaic2"
            alpha 0.2
            additive True

    group effects2:
        # head
        attribute particles:
            "particles_blacksquare"
            xoffset -200 yoffset 200

        # left shoulder
        attribute particles:
            "particles_blacksquare"
            xoffset 1100 yoffset 1500

        # chest
        attribute particles:
            "particles_blacksquare"
            xoffset -1500 yoffset 2000 zoom 1.75
        # attribute particles:
        #     "particles_blacksquare"
        #     xoffset -200 yoffset 1800 zoom 1.25

        # right shoulder
        # attribute particles:
        #     "particles_blacksquare"
        #     xoffset -1200 yoffset 1900 zoom 1.25

        # leg
        # attribute particles:
        #     "particles_blacksquare"
        #     xoffset -700 yoffset 4400 zoom 1.5
        # neck
        # attribute particles:
        #     "particles_blacksquare"
        #     xoffset 1000 yoffset 1200 zoom 0.5
        # attribute particles:
        #     "particles_blacksquare"
        #     xoffset 900 yoffset 2000 zoom 0.75

image josef robot = LayeredImageProxy("josef", Transform(matrixcolor=SaturationMatrix(0.2)))
# image glitched1 = AlphaMask("josef", "mask2")
# image josef_clone = LayeredImageProxy(
#     "josef",
#     [
#         Transform(matrixcolor=BrightnessMatrix(-0.5)),
#         Transform(matrixcolor=SaturationMatrix(0.5))
#     ],
# )

image josef dark = LayeredImageProxy(
    "josef",
    [
        Transform(matrixcolor=BrightnessMatrix(-0.1)),
        Transform(matrixcolor=SaturationMatrix(0.7))
    ],
)

image chair dark:
    "chair"
    matrixcolor BrightnessMatrix(-0.1)
    matrixcolor SaturationMatrix(0.7)
