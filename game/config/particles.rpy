
# Particles

init -2 python:
    from particle import particle_generator

define squaresize = 100
image blacksquare = Solid("#000", xsize=squaresize, ysize=squaresize)
image whitesquare = Solid("#fff", xsize=squaresize, ysize=squaresize)

define square_radius = 500
define square_alpha = 0.5
define square_count = 200
define square_fadetime = 4.0
define square_pos = (2000, 0)

image particle_blacksquare:
    "blacksquare"
    alpha square_alpha
    ease square_fadetime alpha 0.0

image particle_whitesquare:
    "whitesquare"
    alpha square_alpha
    ease square_fadetime alpha 0.0

image particles_whitesquare = particle_generator(
    "particle_whitesquare",
    pos=square_pos,
    radius=square_radius,
    count=square_count,
    start=0,
    uniform=False,
    minspeed=50,
    speed=(-200, 200))

image particles_blacksquare = particle_generator(
    "particle_blacksquare",
    pos=square_pos,
    radius=square_radius,
    count=square_count,
    start=0,
    uniform=False,
    minspeed=50,
    speed=(-200, 200))

define rect_colour = "#fff"
image rectangle_vertical = Solid(rect_colour, xsize=250, ysize=screen_height)
image rectangle_vertical_small = Solid(rect_colour, xsize=500, ysize=int(screen_height/2))
image rectangle_horizontal = Solid(rect_colour, xsize=screen_width, ysize=20)

image particle_soft:
    "particle"
    additive True
    alpha 1.0
    yalign 0.5
    ease 2.0 alpha 0.0

define cyan = "#8aa"
define yellow = "#aa8"
define magenta = "#a8a"

image particle_fade_vertical:
    "rectangle_vertical"
    additive True
    choice:
        matrixcolor ColorizeMatrix(cyan, cyan)
    choice:
        matrixcolor ColorizeMatrix(yellow, yellow)
    choice:
        matrixcolor ColorizeMatrix(magenta, magenta)
    alpha 1.0
    yalign 0.5
    ease 0.5 alpha 0.0

image particle_fade_vertical_small:
    "rectangle_vertical_small"
    additive True
    choice:
        matrixcolor ColorizeMatrix(cyan, cyan)
    choice:
        matrixcolor ColorizeMatrix(yellow, yellow)
    choice:
        matrixcolor ColorizeMatrix(magenta, magenta)
    alpha 1.0
    yalign 0.5
    ease 0.5 alpha 0.0

image particle_fade_horizontal:
    "rectangle_horizontal"
    additive True
    alpha 1.0
    ease 0.2 alpha 0.0

define speed = (-200, 200)
define minspeed = 50
define count = 100
define particles_radius = 50
define particles_count = 100

image particles_soft = particle_generator(
    "particle_soft",
    pos=(screen_width/2 -50, screen_height/2 -200),
    radius=500,
    count=count,
    start=0,
    uniform=False,
    minspeed=minspeed,
    speed=speed)

image particles_top = particle_generator(
    "particle_fade_vertical_small",
    pos=((screen_width/2) - 250, 100),
    radius=particles_radius,
    count=particles_count,
    start=0,
    uniform=False,
    minspeed=minspeed,
    speed=speed)

image particles_left = particle_generator(
    "particle_fade_vertical",
    pos=(-125, 0),
    radius=particles_radius,
    count=particles_count,
    start=0,
    uniform=False,
    minspeed=minspeed,
    speed=speed)

image particles_right = particle_generator(
    "particle_fade_vertical",
    pos=(screen_width - 100, 0),
    radius=particles_radius,
    count=particles_count,
    start=0,
    uniform=False,
    minspeed=minspeed,
    speed=speed)
