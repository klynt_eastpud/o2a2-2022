
define alt_saybox = False
default glasses = True

label start:

    $ alt_saybox = True

    scene black with long_dissolve
    scene lab_blur_light with long_dissolve

    # visiting josef in hospital
    Karel "Are you sure you're all right?"

    show josef zorder 1:
        alpha 0.0
        xalign 0.6
        yalign 0.3
        ease 3.0 yalign 0.14 alpha 1.0

    show chair zorder 0:
        alpha 0.0
        xalign 0.6
        yalign 0.3
        ease 3.0 yalign 0.14 alpha 1.0

    N "I almost can't bear to look at him."
    N "He looks paler than usual - {w}sickly. {w}Almost like a corpse."
    Josef "I'm fine. I was just careless."
    Karel "Josef, you {i}collapsed.{/i} I don't think {i}care{/i} had anything to do with it."

    show josef eb_sad m_open e_closed zorder 1 at middle_laughing
    show chair zorder 0 at middle
    with dissolve

    N "He starts to laugh and quickly devolves into coughing, raising an arm to cover his mouth with his sleeve. {w}When he lowers it again it's specked with blood."
    Karel "They said you can go home...?"
    show josef at middle_breathing with dissolve
    N "He takes a moment to calm his breathing."
    # N "Just watching him is making me anxious. I end up slowing my breathing along with him:"
    # N "In. {w}Out. {w}In. {w}Out. {w}Stabilising."
    show josef m_smile e_closed_smile at middle with dissolve
    # very strong drugs
    Josef "Yes! {w}They've given me a very potent cocktail of drugs to keep me going. I'll be fine."
    N "\"I'll be fine.\" {w}I've started to hate that phrase."
    N "Nine times out of ten it's false. {w}{i}Especially{/i} when he says it."
    Josef "I can't leave my research unfinished after all."
    Karel "Research!? {w}But your health!"
    show josef m_open e_wide eb_raised with dissolve
    Karel "You shouldn't be working! You should be resting!"
    show josef eb_sad m_smile e_closed_smile at middle_laughing with dissolve
    N "He chuckles softly, breath still rattly."
    show josef e_smile at middle with dissolve
    Josef "If I don't complete my research I'll {i}die{/i}, Karel."
    Karel "Oh come on! Don't be so dramatic!"
    # Karel "The world of engineering will happily go on without you."
    show josef m_normal with dissolve
    Josef "Karel, I..."
    show josef e_closed with dissolve
    N "He shakes his head lightly."
    Josef "You don't understand..."
    show josef eb_frown m_normal e_closed at middle with dissolve
    Josef "I should have told you before and... and I'm so sorry I didn't, but..."
    N "A knot forms in my stomach. I can tell something painful is coming."
    N "I don't want to hear it."
    N "I want to run. {w}I want to run out of this hospital room right now and never give him the chance to say what he's going to say next."
    N "But I don't. {w}I can't."
    show josef m_open e_normal at middle with dissolve
    Josef "It's..."
    show josef m_normal at middle with dissolve
    N "He swallows hard and I can't tear my eyes away from him."
    show josef e_side eb_sad at middle with dissolve
    Josef "It's terminal."
    Josef "I'm... {w}I'm going to die."
    N "I play those phrases back what feels like a hundred times in the split-second after hearing them."
    N "I can't parse it. {w}Or my brain refuses to."
    # Karel "Then... {w}You... {w}I..."
    N "I feel like my mind is short-circuiting."
    Josef "So, you see... {w}I {i}have{/i} to finish my work."
    Karel "What...?"
    show josef e_cry m_smile at middle with dissolve
    Josef "I should see this as an opportunity - the first man to inhabit a machine!"
    Karel "Josef..."
    Karel "There has to be another way."
    Josef "No. {w}If I'm going to die anyway, even if the procedure fails, it's the same outcome."
    show josef m_open at middle with dissolve
    Josef "Please. {w}Help me finish the algorithm."
    Karel "No..."
    Karel "No!"
    Karel "I won't be part of this! I won't contribute to the cause of your death!"

    jump the_automaton

label the_automaton:

    scene black with long_dissolve
    $ renpy.pause(1.0, hard=True)
    show lab_blur_dark with long_dissolve
    $ alt_saybox = False
    $ glasses = False

    show chair zorder 1 at middle_far
    show josef robot zorder 2 at middle_far
    with dissolve

    Karel "Josef?"
    N "No response."
    Karel "Josef can you hear me?"
    with vpunch
    N "I roughly shake the automaton by the shoulder and it sways limply in its seat."
    N "If it were a human, I'd call it catatonic."
    N "The reality is far more perverse."
    N "Why'd he have to make it look like him?"
    N "Why'd he have to use a humanoid robot?"
    N "His funeral was last week. {w}I feel like I'm shouting at a corpse."
    Karel "Gods damn it, {i}Josef!{/i}"
    with hpunch
    N "I smack the automaton across the face and the sound reverberates through the lab."
    N "It still sits there, completely unresponsive, head now tilted slightly to the side."
    Karel "I know they all said there's nothing in there - {w}that you're just an empty shell - {w}but I don't believe it. {w}I refuse to."
    Karel "You believed in your research. You were willing to die for it."
    Karel "You {i}have{/i} to still be in there. {w}I feel it in my gut. {w}In my heart."
    Karel "I'm going to get you out."

    jump into_the_machine

label into_the_machine:
    $ alt_saybox = True
    scene black with long_dissolve
    $ renpy.pause(1.0, hard=True)
    scene lab with long_dissolve

    N "Josef's algorithm is loaded up onto the server while the automaton sits in a corner of the lab, still unresponsive, hooked up to the monitoring equipment."
    N "The modifications I made will allow me to \"shell in\" to the automaton with my mind, enabling me to navigate the machine as if inside a dream."
    N "I pick up the helmet and put it on, then activate the sensors. Immediately I see readouts filling the computer screen."
    N "No errors. {w}All systems green."
    N "I take a deep breath, {w}then hit enter."

    scene lab with short_pixellate
    scene lab with pixellate
    scene black with zoom_in_slow

    $ alt_saybox = False

    play music "audio/music.mp3" volume 0.5

    N "Gods, it's cold here."
    N "Even disconnected from my own flesh I can still feel it somehow."
    Karel "Josef!"
    N "Silence stretches on infinitely."
    N "Just calling out feels like a huge effort, like my lungs are filled with tar."
    Karel "Josef! Can you hear me!?"
    N "I'm met with silence again."
    N "Was I wrong?"
    N "Is he really in here?"
    N "Am {i}I{/i} really in here? {w}Or am I...? {w}Is this...?"
    N "I don't want to finish that thought."
    N "No. Josef put himself here to {i}escape{/i} death. He {i}has{/i} to be here somewhere."

    python:
        from maze import Maze

        dead_end_events = [
            "dead_end1",
            "dead_end2",
            "dead_end3",
        ]
        maze = Maze(7, 7, dead_end_events)
        while len(maze.dead_ends) != 3:
            maze = Maze(7, 7, dead_end_events)

    jump the_maze

label after_maze:
    $ renpy.block_rollback()
    $ alt_saybox = False
    $ glasses = True
    scene black

    N "I spot a figure in the distance."

    show josef dark eb_sad e_smile glitched particles zorder 1 at middle_very_far
    show chair dark zorder 0 at middle_very_far
    with dissolve

    # Karel "Josef...?"
    N "No. This is wrong."
    Karel "What happened to you?"

    show josef m_open zorder 1 at middle_far
    show chair zorder 0 at middle_far
    with dissolve

    Josef "Ka...{w}rel..."
    N "His voice is broken, {w}distorted. {w}Like a machine."
    Karel "We have to get you out of here! There must be something wrong with the automaton!"
    Josef "No, Karel. {w}I was... {w}wrong..."
    Karel "Wrong? {w}What are you talking about?"

    show josef e_closed m_open
    with dissolve

    Josef "Humans... {w}were never meant... {w}to inhabit machines..."
    show chair at middle
    show josef e_smile at middle with dissolve
    Josef "Please..."
    Josef "Destroy... {w}the automaton..."
    show josef m_smile e_smile with dissolve
    Josef "Kill... {w}me..."
    Karel "What!?"
    Karel "But your research...!"
    Karel "After everything you did!? Everything you went through?"
    show josef m_open e_cry with dissolve
    Josef "Karel... {w}please..."
    Karel "No! {w}No, I won't do it!"

    show josef_face zorder 0:
        alpha 0.0
        ease 3.0 alpha 0.5

    N "Suddenly, there's a distant rumbling like a train approaching."
    Karel "W-what...?"
    Josef "The machine..."
    N "The rumbling gets louder. I can feel it through my entire body."
    Karel "What? What do you mean!?"
    Josef "The machine!"
    Karel "What's happening!?"

    hide chair
    show josef e_wide2 m_scream static pixels1 -particles at middle_very_close_shake
    with dissolve

    stop music
    play sound "audio/scream.mp3" volume 0.2

    Josef "KILL ME!"

    python:
        repeated = ""
        for i in range(0, 14):
            repeated += "KILL ME! "

    Josef "[repeated]"

    python:
        repeated = ""
        for i in range(0, 42):
            repeated += "KILL ME! "

    Josef "[repeated]"
    jump ending

label ending:

    with flashwhite
    with flashwhite
    scene lab_blur_light with flashwhite

    N "I'm hurled back into the physical world and find myself on the floor next to my chair, helmet askew, cables everywhere."
    N "I take a moment to catch my breath and try to process what just happened."
    N "The automaton. {w}Josef."
    N "That {i}scream{/i}."
    N "As I go over it again and again, one thought keeps bubbling up to the surface."
    N "It's a thought that, as a scientist, terrifies me."
    N "\"Was that real?\""
    N "And I find that I don't know the answer."
    N "All I have is my own memories, as intrinsically fallible as they are."

    $ glasses = False
    show chair zorder 0 at middle_very_far
    show josef robot zorder 1 at middle_very_far with dissolve

    N "I glance at the automaton still sitting in the corner of the lab and find myself instinctively backing away."
    # N "What was it that I saw inside its bio-electronic brain...?"
    N "Who are you...?"

    $ renpy.pause(1.0, hard=True)
    scene black
    with Dissolve(2.0)
    with Pause(2.0, hard=True)

    return
