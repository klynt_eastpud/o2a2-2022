
label dead_end1:
    $ renpy.block_rollback()
    show particles_soft
    N "Hm? There's something here."
    Someone "..arel?"
    # Karel "Josef?"
    Karel "Josef! Can you hear me?"
    hide particles_soft with dissolve
    N "Before I can do anything it's gone. {w}Whatever it was."
    N "I have to keep looking."
    jump the_maze

label dead_end2:
    $ renpy.block_rollback()
    show particles_soft
    N "What's this...?"
    N "I reach towards the strange disturbance but I can't grab hold of anything."
    N "An unusual tingling sensation runs through my hand."
    hide particles_soft with dissolve
    N "It's gone..."
    jump the_maze

label dead_end3:
    $ renpy.block_rollback()
    show particles_soft
    N "Another artifact...?"
    N "I quickly reach towards it, trying to grab it with both hands."
    hide particles_soft with dissolve
    N "It flickers and disappears."
    jump the_maze

# label visited1:
#     $ renpy.block_rollback()
#     N "I feel like I've been this way before."
#     jump the_maze
#
# label visited2:
#     $ renpy.block_rollback()
#     N "All right, I've {i}definitely{/i} been this way before."
#     jump the_maze
#
# label visited3:
#     $ renpy.block_rollback()
#     N "I'm going round in circles..."
#     jump the_maze

label the_maze:
    $ alt_saybox = False
    scene black with dissolve

    while not maze.solved:
        menu (maze_choice=True):
            "Go fowards" ("FORWARD") if "FORWARD" in maze.open_directions:
                python:
                    maze.move("FORWARD")
            "Go left" ("LEFT") if "LEFT" in maze.open_directions:
                python:
                    maze.move("LEFT")
            "Go right" ("RIGHT") if "RIGHT" in maze.open_directions:
                python:
                    maze.move("RIGHT")
            "Go backwards" ("BACKWARD") if "BACKWARD" in maze.open_directions:
                python:
                    maze.move("BACKWARD")

    jump after_maze
